CREATE DATABASE  IF NOT EXISTS `payroll` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `payroll`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: payroll
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee` (
  `idEmployee` int NOT NULL,
  `Employee_Number` int unsigned NOT NULL,
  `Last_Name` varchar(45) NOT NULL,
  `First_Name` varchar(45) NOT NULL,
  `SSN` decimal(10,0) DEFAULT NULL,
  `Pay_Rate` varchar(40) DEFAULT NULL,
  `Pay_Rates_idPay_Rates` int NOT NULL,
  `Vacation_Days` int DEFAULT NULL,
  `Paid_To_Date` decimal(2,0) DEFAULT NULL,
  `Paid_Last_Year` decimal(2,0) DEFAULT NULL,
  PRIMARY KEY (`Employee_Number`,`Pay_Rates_idPay_Rates`),
  UNIQUE KEY `Employee_Number_UNIQUE` (`Employee_Number`),
  KEY `fk_Employee_Pay_Rates` (`Pay_Rates_idPay_Rates`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (111,11,'Nguyen','Phu',NULL,'10.000.000',1,2,NULL,NULL),(112,12,'Le','Hoan',NULL,'12.000.000',2,3,NULL,NULL),(113,13,'Nguyen','Toi',NULL,'5.000.000',3,0,NULL,NULL),(114,14,'Nguyen','Ni',NULL,'3.000.000',4,4,NULL,NULL),(115,15,'Huynh','Thuong',NULL,'2.000.000',5,3,NULL,NULL),(116,16,'Thanh','Ngan',NULL,'4.500.000',6,1,NULL,NULL),(117,17,'Huynh','Nguyet',NULL,'2.000.000',5,2,NULL,NULL),(118,18,'Nguyen','Tu',NULL,'5.000.000',3,3,NULL,NULL),(119,19,'Nguyen','Kieu',NULL,'12.000.000',2,4,NULL,NULL),(120,20,'Nguyen','Thien',NULL,'18.000.000',2,5,NULL,NULL),(121,21,'Nguyen','Tuong',NULL,'10.000.000',1,6,NULL,NULL),(122,22,'Hoyer','Jade',NULL,'5.000.000',3,2,NULL,NULL),(123,23,'Hoyer','Dway',NULL,'3.000.000',4,4,NULL,NULL),(124,24,'Hoyer','Ngoc',NULL,'2.000.000',5,5,NULL,NULL),(125,25,'Huynh','Binh',NULL,'12.000.000',2,3,NULL,NULL),(126,26,'Nguyen','Phuong',NULL,'5.000.000',3,2,NULL,NULL),(127,27,'Hoai','Nguyen',NULL,'12.000.000',2,4,NULL,NULL),(128,28,'Tran','Trang',NULL,'5.000.000',3,2,NULL,NULL),(129,29,'Nguyen','Tram',NULL,'12.000.000',2,1,NULL,NULL),(130,30,'Phan','Loc',NULL,'2.000.000',5,4,NULL,NULL),(131,31,'Nguyen','Vy',NULL,'12.000.000',2,2,NULL,NULL),(132,32,'Phan','Van',NULL,'12.000.000',2,3,NULL,NULL),(133,33,'Phan','Linh',NULL,'2.000.000',5,4,NULL,NULL),(134,34,'Phan','Hoa',NULL,'12.000.000',2,5,NULL,NULL),(135,35,'Phan','Hung',NULL,'10.000.000',1,2,NULL,NULL),(136,36,'Le','Nguyet',NULL,'10.000.000',1,1,NULL,NULL),(137,37,'Nguyen','Chanh',NULL,'5.000.000',3,3,NULL,NULL),(138,38,'Nguyen','Hieu',NULL,'2.000.000',5,4,NULL,NULL),(139,39,'Nguyen','Yen',NULL,'5.000.000',3,2,NULL,NULL),(140,40,'Nguyen','Dung',NULL,'12.000.000',2,1,NULL,NULL),(141,41,'Nguyen','Phuoc',NULL,'3.000.000',4,4,NULL,NULL),(142,42,'Nguyen','An',NULL,'12.000.000',2,7,NULL,NULL),(143,43,'Truon','Trinh',NULL,'12.000.000',2,6,NULL,NULL),(144,44,'Nguyen','Man',NULL,'5.000.000',3,3,NULL,NULL),(145,45,'Vo','Anh',NULL,'12.000.000',2,2,NULL,NULL),(146,46,'Nguyen','Trieu',NULL,'3.000.000',4,5,NULL,NULL),(147,47,'Huynh','Dung',NULL,'2.000.000',5,3,NULL,NULL),(148,48,'Bao','Nguyen',NULL,'5.000.000',3,2,NULL,NULL),(149,49,'Nguyen','Yen',NULL,'4.500.000',6,4,NULL,NULL),(150,50,'Tran','Tri',NULL,'5.000.000',3,2,NULL,NULL);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pay_rates`
--

DROP TABLE IF EXISTS `pay_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pay_rates` (
  `idPay_Rates` int NOT NULL,
  `Pay_Rate_Name` varchar(40) DEFAULT NULL,
  `Value` decimal(10,0) DEFAULT NULL,
  `Tax_Percentage` decimal(10,0) DEFAULT NULL,
  `Pay_Type` int DEFAULT NULL,
  `Pay_Amount` decimal(10,0) DEFAULT NULL,
  `PT-Level C` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`idPay_Rates`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pay_rates`
--

LOCK TABLES `pay_rates` WRITE;
/*!40000 ALTER TABLE `pay_rates` DISABLE KEYS */;
INSERT INTO `pay_rates` VALUES (1,'Pay',9000000,1,1,11000000,NULL),(2,'Salary',12000000,1,1,13000000,NULL),(3,'Wage',4000000,1,1,5500000,NULL),(4,'Allowance',2000000,0,1,4000000,NULL),(5,'Bonus',1000000,0,1,2500000,NULL),(6,'Overtime',4000000,0,1,5000000,NULL),(7,'Severance',1000000,1,1,2000000,NULL),(8,'Pension',500000,0,1,3000000,NULL);
/*!40000 ALTER TABLE `pay_rates` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-22 21:07:46
